# Ansible integration in Jenkins 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Use Terraform to automate:
  - Server for Jenkins (DigitalOcean) 
  - Server for Ansible Control node (DigitalOcean)
  - Two servers for Ansible worker nodes (EC2)
* Write Ansible Playbook, which configures 2 EC2 Instances
* Add ssh key file credentials in Jenkins for Ansible Control Node server and Ansible Managed Node servers
* Configure Jenkins to execute the Ansible Playbook on remote Ansible Control Node server as part of the CI/CD pipeline
* The Jenkinsfile configuration will do the following:
  - Connect to the remote Ansible Control Node server
  - Copy Ansible playbook and configuration files to the remote Ansible Control Node server
  - Copy the ssh keys for the Ansible Managed Node servers to the Ansible Control Node server
  - Install Ansible, Python3 and Boto3 on the Ansible Control Node server
  -  With everything installed and copied to the remote Ansible Control Node server, execute the
     playbook remotely on that Control Node that will configure the 2 EC2 Managed Nodes

## Technologies Used 

* Ansible 

* Terraform 

* Jenkins 

* DigitalOcean 

* AWS 

* Boto3 

* Docker

* Java 

* Maven 

* Linux 

* Git 

## Steps 

Step 1: Use terraform to automate the creation of jenkins server 

[Jenkins Server Creation](/images/01_use_terraform_to_automate_the_creation_of_jenkins_server.png)

Step 2: Use terraform to automate the creation of ansible Control node server 

[Ansible Control node server creation](/images/02_use_terraform_to_automate_the_creation_of_ansible_server.png)

Step 3: Ssh into ansible server 

    ssh root@165.227.224.11

[Ssh into Control node first time](/images/03_ssh_into_ansible_server.png)

Step 4: Install the following on the server: 
  - ansible 
  - pip3 
  - boto3 (python module)
  - botocore (python module )

[Ansible install](/images/04_install_ansible_on_the_ansible_server.png)

[pip3 install](/images/05_install_pip3.png)

[Python modules Boto3 and botocore install](/images/06_install_python_modules_boto3_andbotocore.png)

Step 5: Create AWS credentials file in ansible control node server 

    mkdir .aws
    vim .aws/credentials 

[AWS credentials file creation](/images/07_creating_aws_credentials_file_in_ansible_server.png)

Step 6: Insert your credentials in the file 

[inserting aws credential](/images/08_insert_your_credentials_in_this_file.png)

Step 7: Create key pair on aws 

[AWS keypair](/images/09_create_key_pair_on_aws.png)

Step 8: Use terraform to automte aws infastructure, create two instances 

[Terraform config file for aws infastructure](/images/10_use_terraform_to_automate_aws_infastructure_create_two_instances.png)

[Ec2 instances running](/images/11_ec2_instances_running_on_console.png)

Step 9: Edit ansible config file and set inventory to inventory_aws_ec2.yaml which is what contains my plugin setting 

[Ansible config file change inventory](/images/12_edit_the_ansible_cfg_file_and_set_inventory_to_inventory_aws_ec2_this_contains_plugin_setting_set_private_key_path_to_the_correct_path_that_it_will_be_on_the_ansible_control_node_server.png)

Step 10: Create playbook file

[Created playbook file](/images/13_create_playbook.png)

Step 11: Write plays that install the following:
  - Python
  - Docker 
  - Docker-compose 
  - Docker python module 

Note: You can check documentation for the right module to use or you can check my playbook for the module i used
  Modules used:
   - yum: Python3 and Docker 
   - get_url: Docker-compose
   - pip: Docker python module 

Step 12: Write play that starts docker daemon

Note: You can check documentation for the right module to use or you can check my playbook for the module i used
  Modules used:
  - systemd: Start docker daemon 

[Plays in playbook](/images/14_write_plays_that_installs_python_docker_docker-compose_docker_python_module_and_starts_docker_daemon.png)

Step 13: Make four files available on the ansible control node server which are:
  - ssh key file 
  - playbook
  - ansible config file 
  - inventory file for plugin



Step 14: Ssh into jenkins server and start jenkins docker container 

    ssh root@132.227.124.11
    docker start edfr12332

Note: This jenkins server used for this project is a jenkins server i created on one of my previous projects in which i ran jenkins as a docker container on a digitalOcean server. There will be a link of this project in my references.

[Start Jenkins container on Jenkins host server](/images/16_ssh_into_jenkins_server_and_start_jenkins_docker_container.png)

Step 15: Make sure you have the ssh agent plugin installed on Jenkins 

[Ssh agent plugin on Jenkins](/images/17_make_sure_you_have_the_ssh_agent_plugin_installed_on_jenkins.png)

Step 16: Create credentials on Jenkins, give it username of the account you want to have access to when you ssh into ansible control node server which is the root and finally past your ssh private key in there.

Note: Before creating this server i had already given DigitalOcean my public key to enable access to the server, so because of this i copied and pasted the private key pair for the public key in which i gave DigitalOcean.


[Ssh agent plugin Credential Creation](/images/18_key_and_past_private_key_in_there.png)

Step 17: Before pasting key convert ssh private key to classic format to avoid errors on jenkins 

    ssh-keygen-p-f~/.ssh/id_ed25519 -m pem -P "" -N ""

[Converting ssh key to classic format](/images/19_before_pasting_key_convert_ssh_private_key_to_classic_format_to_avoid_errors_on_jenkins.png)

Step 18: Create a Jenkinsfile, in this Jenkinsfile create a pipeline stage that copies the files to ansible control node in Step 13. Insert the ssh agent plugin in this stage 

[Pipeline copy file stage creation](/images/20_create_a_jenkinsfile_in_this_jenkinsfile_create_a_pipeline_stage_that_copies_the_files_to_ansible_control_node_insert_the_plugin_in_this_stage.png)

Step 19: in sshagent plugin block use scp to copy the files to ansible control node in the root user home directory 

[scp in sshagent block](/images/21_in_sshagent_plugin_block_use_scp_to_copy_the_files_to_ansible_control_node_in_the_root_user_home_directory.png)

Step 20: You will need ssh private key for ansible worker nodes to have access to them and execute the playbook on so what i did is copy the private key which i downloaded on my local computer

[ec2 server private key](/images/22_we_will_need_ssh_private_key_for_ansible_worker_nodes_to_have_access_to_them_and_execute_the_playbook_on_so_what_we_need_to_do_is_copy_the_private_key_which_i_have_downloaded_on_my_local_computer.png)

Step 21: Create a global credential in Jenkins that contains private key in which we can use to have access to worker nodes on jenkins 

[Credentials for worker nodes on jenkins](/images/23_create_a_global_credential_in_jenkins_that_contains_private_key_in_which_we_can_use_to_have_access_to_worker_nodes_on_jenkins.png)

Step 22: In the jenkinsfile stage for copying files use another plugin called withCredentials, provide credentialsId keyfile which will be saved as a variable and username

[WithCredentials](/images/24_in_the_jenkinsfile_stage_for_copying_files_use_another_plugin_called_withCredentials_provide_credentialsId_keyfile_which_will_be_saved_as_a_variable_and_username.png)

Step 23: Use scp to copy the file that contains ssh key to ansible control node user directory with appropriate name given in path in ansible cfg file private_key_file.

[scp](/images/25_use_scp_to_copy_the_file_that_contains_ssh_key_to_ansible_control_node_user_directory_with_approriate_name_given_in_path_in_ansible_cfg_file_private_key_file.png)

Step 24: Test Pipeline 

[Pipeline test 1](/images/25_pipeline_output.png)

Step 25: Create a script to automate the manual part of the project which is preparation of ansible control node 

[script for preparing ansible control node server](/images/26_create_a_script_to_automate_the_manual_part_of_the_project_which_is_preparation_of_ansible_control_node.png)

Step 25: Create a new stage in jenkinsfile for executing ansible playbook to configure ec2 instance 

[Jenkinsfile execute stage](/images/31_create_new_stage_in_jenkinsfile_for_executingansible_playbook_to_configure_ec2_instances.png)

Step 26: Go on jenkins and download plugin ssh pipeline steps this will enablejenkins to run commands which means execute the playbook 

[ssh pipeline steps plugins](/images/32_go_on_jenkins_and_download_plugin_ssh_pipeline_steps_this_will_enable_jenkins_to_run_commands_which_means_execute_the_playbook.png)

Step 27: Due to this plugins downloaded we can now execute commands with jenkins on ansible server however to do this the plugin has syntax to folow which  you can check the documentation, first thing to do is create remote object then give necessary attribute like name, host which is ip address of jenkins server. Insert allowAnyHosts to avoid any prompts, give necessary username and identitfy file which you can reference with the withCredentials 

[Pipeline steps plugin 2](/images/33_due_to_this_plugin_downloaded_we_can_now_execute_commands_with_jenkins_on_ansible_server_however_to_do_this_the_plugin_hassyntax_to_follow_which_you_can_check_the_documentation_first_thing_to_do_is_create_remote_object_then_give_necessary_attribute.png)

[Pipeline steps plugin 3](/images/33_like_name_host_which_is_ip_address_of_jenkins_server_ser_allowAnyHosts_to_avoid_any_prompts_give_necessary_username_and_identitfy_file_which_you_can_reference_with_the_withCredentials.png)

Step 28: Use sshcommand to give necessary command you want to execute

[sshcommand](/images/33_and_use_sshCommand_to_give_necessary_command_you_want_to_execute.png)

Step 29: Push changes to gitlab remote repo 

[Push changes](/images/26_push_changes_to_gitlab_remote_repo.png)

Step 30: Create a simple ansible pipeline on Jenkins 

[Creating ansible pipeline](/images/27_create_a_simple_ansible_pipeline.png)

Step 31: Run script and ansible playbook on Jenkinsfile 

[Run script](/images/27_run_script_and_ansible_playbook_on_jenkins_file.png)

Step 32: In the pipelin general configuration in pipeline definition use pipeline script from scm and put repo url and credentials 

[general pipeline configuration](/images/28_in_the_pipeline_general_configuration_in_pipeline_definition_use_pipeline_script_from_scm_and_put_repo_url_and_credentials.png)

Step 33: Test pipeline 

[Success](/images/28_success.png)
[Pipeline success](/images/29_successful_pipeline.png)
[files in ansible](/images/30_files_in_ansible_control_node_server.png)

## Installation

    brew install ansible 

## Usage 

    ansible-playbook -i host deploy-docker.yaml 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/ansible-integration-in-jenkins.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/ansible-integration-in-jenkins

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.
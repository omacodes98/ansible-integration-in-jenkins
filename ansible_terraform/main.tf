provider "digitalocean" {
  token = var.do_token
}

variable region {}
variable do_token {}
variable image_id {}
variable size_id {}
variable ssh_keys {}
variable server_name {}

resource "digitalocean_droplet" "ansible_server" {
    image = var.image_id
    name = "${var.server_name}-SERVER"
    region = var.region
    size = var.size_id
    ssh_keys = var.ssh_keys
}

output "droplet_public_ip" {
    value = digitalocean_droplet.ansible_server.ipv4_address
}
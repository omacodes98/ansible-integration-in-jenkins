provider "digitalocean" {
  token = var.do_token
}

variable size {}
variable droplet_name {}
variable region {}
variable do_token {}
variable image_id {}
variable ssh_keys {}


resource "digitalocean_droplet" "jenkins_server" {
    name = var.droplet_name
    image = var.image_id
    region = var.region
    ssh_keys = var.ssh_keys
    size = var.size
}

output "droplet_public_ip" {
    value = digitalocean_droplet.jenkins_server.ipv4_address
}